package Todo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import static org.junit.Assert.*;

@SuppressWarnings("unchecked")
public class TaskOrganigerTest {

    private TaskOrganiger taskOrganiger;
    private ArrayList<Task> tasks;
    private String testFile="test.json";


    @Before
    public void setUp()
    {
        ArrayList<Task> task= new ArrayList<>();

        Task taskone= new Task("task 1",LocalDate.parse("2020-12-13"),"done","project 1");
        Task tasktwo= new Task("task 2",LocalDate.parse("2020-12-11"),"done","project 1");
        Task taskthree= new Task("task 3",LocalDate.parse("2020-12-10"),"notdone","project 1");
        task.add(taskone);
        task.add(tasktwo);
        task.add(taskthree);
        ArrayList<String> list = new ArrayList<>();
        JSONArray item = new JSONArray();
        for (int i = 0; i < task.size(); i++) {
            JSONObject newinnerObj = new JSONObject();
            newinnerObj.put("Task Titel:", task.get(i).getTitel());
            newinnerObj.put("Due Date:", task.get(i).getDate().toString());
            newinnerObj.put("Task Status:", task.get(i).getStatus());
            newinnerObj.put("Project Name:", task.get(i).getProjectName());
            item.add(newinnerObj);
        }
        JSONObject outerObject = new JSONObject();
        outerObject.put("tasks", item);
        list.add(outerObject.toJSONString());
        try{
            Files.write(Paths.get(testFile), list);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        taskOrganiger = new TaskOrganiger(testFile);
        tasks = taskOrganiger.getTaskList();

    }

    @After
    public void tearDown() throws Exception {
        try {
            File file =new File(testFile);
            file.delete();//delete the file named testFile.json
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void testReadTaskReadingFromTheFile() {
        int taskSize=tasks.size();
        assertEquals(3,taskSize);
    }

    @Test
    public void testReadTaskReadingFromTheFileNotEmpty() {
        int taskSize=tasks.size();
        assertFalse(taskSize ==0);
    }

    @Test
    public void CountAllTasksAndDoneTasksTest(){
        int count =  tasks.size();
        int done = (int) tasks.stream()
                .filter(s -> "done".equals(s.getStatus()))
                .count();
        String testString=">>> You have total: " + count + " Tasks and You have completed : " + done + " Tasks";
        assertEquals(testString, taskOrganiger.displayTaskStatus());

    }
    @Test
    public void testInsertSingleTask() throws Exception{
        //TaskOrganiger taskOrganiger=new TaskOrganiger();
        //ArrayList<Task> tasks =taskOrganiger.readTask();
        int testSize=tasks.size();
        Task task= new Task("Task 23", LocalDate.parse("2020-12-18"),"active","project 5");
        taskOrganiger.insertTask(task,testFile);
        ArrayList<Task> updatedTask =taskOrganiger.readTask(testFile);
        int currentSize=updatedTask.size();
        assertEquals(testSize+1,currentSize);

    }


    @Test
    public void testShowAllTask(){
        ArrayList<Task> tasks = taskOrganiger.showAllTheTask();
        int taskSize=tasks.size();
        assertEquals(3,taskSize);

    }

    @Test
    public void testWriteTask(){

        Task taskone= new Task("task 1",LocalDate.parse("2020-12-13"),"done","project 1");
        Task tasktwo= new Task("task 2",LocalDate.parse("2020-12-11"),"done","project 1");
        Task taskthree= new Task("task 3",LocalDate.parse("2020-12-10"),"notdone","project 1");
        tasks.add(taskone);
        tasks.add(tasktwo);
        tasks.add(taskthree);
        taskOrganiger.writeTask(tasks,testFile);

        assertEquals(6,taskOrganiger.getTaskList().size());

    }

    @Test
    public void testRemoveTask(){
        tasks.remove(0);
        assertEquals(2,tasks.size());

    }

}

package Todo;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * This is a simple task class.
 * this class maintains information of a task.
 * @author  Shakir Ahmed Zahedi
 * @version 1.0
 * @since   2020-03-10
 */

@SuppressWarnings("unchecked")
public class Main {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
    private static String fileName="one.json";



    public static Task addNewTask(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a Task Name: ");
        Task task = new Task();
        task.setTitel(scanner.nextLine());
        System.out.println("Enter a Due Date:(yyyy-MM-dd) ");
        LocalDate date=LocalDate.now();
        String k = scanner.nextLine();
        try {
             date = LocalDate.parse(k, formatter);


        }
        catch (Exception e){
            System.out.println(e.getMessage());

        }
        task.setDate(date);

        System.out.println("Enter status: ");
        task.setStatus(scanner.nextLine());
        System.out.println("Enter a Project Name: ");
        task.setProjectName(scanner.nextLine());
        return task;
    }



    public static void main(String[]args){
         String fileName="one.json";


        TaskOrganiger taskOrganiger= new TaskOrganiger(fileName);
        Scanner scanner=new Scanner(System.in);
        System.out.println(">> Welcome to ToDoLy");
        System.out.println(taskOrganiger.displayTaskStatus());
        boolean found=true;
        while (found) {
            System.out.println(">> Do you want to Continue:\n"+
                    ">> press 'y' to yes and press 'n' to no\n");
            char continueOption = scanner.next().charAt(0);
            if(continueOption=='y') {
                System.out.println(taskOrganiger.displayTaskStatus());

                System.out.println("*******************************************************************'");
                System.out.println(">> Pick an option:\n" +
                        ">> (1) Show Task List (by date or project)\n" +
                        ">> (2) Add New Task\n" +
                        ">> (3) Edit Task (update, mark as done, remove)\n" +
                        ">> (4) Save and Quit");
                System.out.println("*******************************************************************'");


                int choice = scanner.nextInt();
                if (choice == 1) {
                    ArrayList<Task> tasks = taskOrganiger.showAllTheTask();
                    for (int index = 0; index < tasks.size(); index++) {
                        System.out.println((index + 1) + ".\t" + tasks.get(index));
                    }
                    found = true;

                } else if (choice == 2) {
                    taskOrganiger.insertTask(addNewTask(),fileName);
                    found = true;
                } else if (choice == 3) {
                    ArrayList<Task> tasks = taskOrganiger.showAllTheTask();
                    for (int index = 0; index < tasks.size(); index++) {
                        System.out.println((index + 1) + ".\t" + tasks.get(index));
                    }

                    System.out.println("Enter the task No you want to Edit/Delete: ");
                    int inputlineNo = scanner.nextInt();
                    Task editObject = tasks.get(inputlineNo - 1);

                    System.out.println("*******************************************************************'");
                    System.out.println(">> Pick an option:\n" +
                            ">> (1) To Edit Task Titel :\n" +
                            ">> (2) To Edit Task DueDate:\n" +
                            ">> (3) To Edit Task Status:\n" +
                            ">> (4) To Edit Project Name:\n" +
                            ">> (5) To Delet the Task:");
                    System.out.println("*******************************************************************'");
                    int choiceOfEdit = scanner.nextInt();

                    String titel, date, status, projectName;
                    if (choiceOfEdit == 1) {
                        System.out.println("Enter the Task Titel:\n");
                        titel = scanner.next();
                        editObject.setTitel(titel);
                    } else if (choiceOfEdit == 2) {
                        System.out.println("Enter the Task Duedate:\n");
                        date = scanner.next();
                        editObject.setDate(LocalDate.parse(date, formatter));

                    } else if (choiceOfEdit == 3) {
                        System.out.println("Enter the Task Status:");
                        status = scanner.next();
                        editObject.setStatus(status);

                    } else if (choiceOfEdit == 4) {
                        System.out.println("Enter the Task ProgramName:\n");
                        projectName = scanner.nextLine();
                        editObject.setProjectName(projectName);
                    } else if (choiceOfEdit == 5) {
                        //System.out.println("Enter the Task ProgramName:\n");
                        //delete = scanner.next();
                        taskOrganiger.removeTask(inputlineNo - 1);
                        //tasks.remove(inputlineNo - 1);
                    }

                    taskOrganiger.save(fileName);
                    found = true;


                } else if (choice == 4) {
                    scanner.close();
                    found = false;
                    // System.exit(0);
                }
                else {
                    System.out.println("You need to enter a valid key!");
                    found = true;
                }
            }
            else if (continueOption=='n') {
                found = false;
                break;

            }
            else {
                System.out.println("You need to enter a valid key!");
                found = true;
            }
            }
        System.out.println("*********************************************************************");
        System.out.println("Thank you for use the Application");
        System.out.println("*********************************************************************");
        scanner.close();
        System.exit(0);
    }
}

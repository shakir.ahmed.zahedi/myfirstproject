package Todo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
/**
 * TaskOrganiger model is the responsible to manage task.
 * This model able to creat,add,edit,view task.
 * tasks are all sorted according to duedate.
 * this class maintains information of a task.
 * @author  Shakir Ahmed Zahedi
 * @version 1.0
 * @since   2020-03-10
 */
@SuppressWarnings("unchecked")
public class TaskOrganiger {
    // fields...
    // create a arraylist of Task.
    private ArrayList<Task> tasks;
    // to formate the date.
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");

    /**
     * @Constructor
     */

    public TaskOrganiger(String fileName) {

        tasks = new ArrayList<>();
        readTask(fileName);
    }
    /**
     *
     * @return Array<Task>
     */

    public ArrayList<Task> getTaskList()
    {

        return tasks;
    }
    /**
     *
     * @param filename
     * read all the Task from the filename
     * @return Array<Task>
     */

    public ArrayList<Task> readTask(String filename) {
        tasks.clear();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        try {
            jsonObject = (JSONObject) parser.parse(new FileReader(filename));
            jsonArray = (JSONArray) jsonObject.get("tasks");

        } catch (NullPointerException e) {
            System.out.println("The file is empty" + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject ami = (JSONObject) jsonArray.get(i);
                ArrayList<String> a = new ArrayList<>(ami.values());
                tasks.add(new Task(a.get(2), LocalDate.parse(a.get(1), formatter), a.get(3), a.get(0)));
            }
            return (ArrayList<Task>) tasks;

        }
    }

    /**
     *
     *
     * @param  allTasks,fileName
     *  sort the list and
     *   write the list on a file as json object.
     */

    public void writeTask(ArrayList<Task>allTasks,String fileName){
        ArrayList<String> list = new ArrayList<>();
        JSONArray item = new JSONArray();
        List<Task> sortedList = allTasks.stream()
                .sorted(Comparator.comparing(Task::getDate))
                .collect(Collectors.toList());

        for (int i = 0; i < sortedList.size(); i++) {
            JSONObject newinnerObj = new JSONObject();
            newinnerObj.put("Task Titel:", sortedList.get(i).getTitel());
            newinnerObj.put("Due Date:", sortedList.get(i).getDate().toString());
            newinnerObj.put("Task Status:", sortedList.get(i).getStatus());
            newinnerObj.put("Project Name:", sortedList.get(i).getProjectName());
            item.add(newinnerObj);
        }
        JSONObject outerObject = new JSONObject();
        outerObject.put("tasks", item);
        list.add(outerObject.toJSONString());
        try{
            Files.write(Paths.get(fileName), list);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        //Files.write(Paths.get(fileName), list);
        System.out.println("Task is Update succesfully");
    }
    /**
     *
     * @param  task
     *  This method should use to read the exiting json file
     *  and add a task to arraylist and update the json file.
     */

    public void insertTask(Task task,String fileName){
        tasks.add(task);
        writeTask(tasks,fileName);
    }

    /**
     *
     *  This method should use to read the exiting json file
     *  and
     *  @return ArrayList of Task.
     */

    public ArrayList<Task> showAllTheTask() {
        List<Task> sortedList = tasks.stream()
                .sorted(Comparator.comparing(Task::getDate))
                .collect(Collectors.toList());

        return (ArrayList<Task>) sortedList;
    }

    /**
     *
     * @return String
     *  This method should use to print the number of task in the exiting json file.
     *  and show the number of done task in the exiting json file.
     */

    public String displayTaskStatus() {
        int count = (int) tasks.stream()
                .count();
        int done = (int) tasks.stream()
                .filter(s -> "done".equals(s.getStatus()))
                .count();
        return ">>> You have total: " + count + " Tasks and You have completed : " + done + " Tasks";

    }
    /**
     *
     * @param  index
     *  This method should use to remove a Task
     *   to arraylist.
     */

    public void removeTask(int index)
    {
        tasks.remove(index);

    }
    /**
     *
     * @param  fileName
     *  This method save the change of the file
     *
     */

    public void save(String fileName)
    {
        writeTask(tasks,fileName);
    }
}
